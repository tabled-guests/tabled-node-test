const router = require('express').Router()
const User = require('../models/users-model.js')

// READ ALL USERS
router.get('/', async (req, res) => {
  try {
    const users = await User.readUsers()
    res.status(200).json(users)
  } catch (err) {
    res.status(500).json({ err: 'Error while reading users' })
  }
})

// READ USER BY ID
router.get('/:id', async (req, res) => {
  const userId = req.params.id
  try {
    const user = await User.readUser(userId)
    if (!user) {
      res.status(404).json({ err: 'The user with the specified id does not exist' })
    } else {
      res.status(200).json(user)
    }
  } catch (err) {
    res.status(500).json({ err: 'Error while reading the user' })
  }
})

// CREATE USER
router.post('/', async (req, res) => {
  const newUser = req.body
  if (!newUser.name) {
    res.status(400).json({ err: 'Please provide the name' })
  } else {
    try {
      const user = await User.createUser(newUser)
      res.status(200).json(user)
    } catch (err) {
      res.status(500).json({ err: 'Error while creating user' })
    }
  }
})

// UPDATE USER
router.put('/:id', async (req, res) => {
  const userId = req.params.id
  const newUser = req.body
  if (!newUser.name) {
    res.status(404).json({ err: 'You are missing information' })
  } else {
    try {
      const updatedUser = await User.updateUser(userId, newUser)
      res.status(200).json(updatedUser)
    } catch (err) {
      res.status(500).json({ err: 'Error in updating user' })
    }
  }
})

// DELETE USER
router.delete('/:id', async (req, res) => {
  const userId = req.params.id
  try {
    const userToDelete = await User.deleteUser(userId)
    res.status(204).json(userToDelete)
  } catch (err) {
    res.status(500).json({ err: 'Error in deleting user' })
  }
})

module.exports = router
