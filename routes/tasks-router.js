const router = require('express').Router()

const Task = require('../models/tasks-model.js')

// READ ALL TASKS
// todo: how would you improve the path to this endpoint?
router.get('/read-tasks', async (req, res) => {
  // todo: complete the handler.
  // may respond with status 500
})

// READ TASK BY ID
router.get('/:id', async (req, res) => {
  const taskId = req.params.id
  // todo: complete the handler.
  // may respond with status 404 or 500
})

// CREATE TASK
router.post('/', async (req, res) => {
  const newTask = req.body
  // todo: complete the handler.
  // may respond with status 400 or 500
  // `newTask` must include `title`
  // respond with the id of the newly created task
})

// UPDATE TASK
router.put('/:id', async (req, res) => {
  const taskId = req.params.id
  const newTask = req.body
  // todo: complete the handler.
  // may respond with status 400 or 500
  // `newTask` must include `title`
  // respond with no data
})

module.exports = router
